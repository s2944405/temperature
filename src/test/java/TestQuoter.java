import nl.utwente.di.temperature.Quoter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestQuoter {

    @Test
    public void testTemperature() throws Exception {
        Quoter quoter = new Quoter();
        double fahrenheit = quoter.getFahrenheit("30");
        Assertions.assertEquals(86.0, fahrenheit, 0.0, "Price of book 1");
    }

}
