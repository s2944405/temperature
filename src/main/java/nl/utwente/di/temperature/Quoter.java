package nl.utwente.di.temperature;

public class Quoter {

    public double getFahrenheit(String celsius) {
        int celsiusInt = Integer.parseInt(celsius);
        double fahrenheit = celsiusInt * 1.8 + 32;
        return fahrenheit;
    }

}
